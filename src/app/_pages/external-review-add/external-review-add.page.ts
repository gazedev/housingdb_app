import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { ApiService } from '_services/api.service';
import { AlertService } from '_services/alert.service';
import { AuthenticationService } from '_services/index';
import { UrlValidator } from '_validators/url-validator';


@Component({
  selector: 'external-review-add-page',
  templateUrl: './external-review-add.page.html',
  styleUrls: ['./external-review-add.page.scss'],
})
export class ExternalReviewAddPage implements OnInit {

  @ViewChild('ngFormDirective') formDirective;
  form: FormGroup;
  submitAttempt: boolean;
  currentlySubmitting: boolean;
  reviewableType: string;
  reviewableId: string;
  externalReview: object;
  breadcrumbs: any;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private alertService: AlertService,
    public authService: AuthenticationService,
  ) {
    this.submitAttempt = false;
    this.currentlySubmitting = false;
    this.form = this.formBuilder.group({
    
      date: ['', Validators.compose([Validators.required])],
      url: ['', Validators.compose([Validators.required, UrlValidator])],
    });
  }

  async ngOnInit() {
    await this.authService.checkLogin();
    this.breadcrumbs = ['Add External Review'];
    this.route.queryParamMap.subscribe(params => {
      this.reviewableType = params.get('reviewableType');
      this.reviewableId = params.get('reviewableId');
      this.getReviewable();
    });
  }

  submit() {
    this.currentlySubmitting = true;
    this.submitAttempt = true;
    if(!this.form.valid) {
      return;
    }

    this.externalReview = {url: this.form.value.url, date: this.form.value.date}

    this.apiService.addExternalReview(this.reviewableType, this.reviewableId, this.externalReview).subscribe(
      res=>{
        this.alertService.success('The external review has been created.');
        // Navigate back to details page to prevent multiple submissions of same external review
        this.router.navigate([`${this.reviewableType}/${this.reviewableId}`], {
          fragment: res.id,
        });
      },
      err=>{console.log('error creating external review', err);
    });
      
  }

  getReviewable() {
    if (this.reviewableType == "property") {
      this.getProperty();
    } else if (this.reviewableType == "landlord") {
      this.getLandlord();
    }
  }

  getProperty() {
    this.apiService.getProperty(this.reviewableId).subscribe(res => {
      this.setSelection(this.reviewableType, res);
    },
    err => {
      console.log('error getting property', err);
    });
  }

  getLandlord() {
    this.apiService.getLandlord(this.reviewableId).subscribe( res => {
      this.setSelection(this.reviewableType, res);
    },
    err => {
      console.log('error getting landlord', err)
    });
  }
  
  setSelection(type, item) {
    this.reviewableType = type;
    this.reviewableId = item.id;
  }
}
