import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, ApiService, HeadService } from '_services/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Landlord } from '_models/landlord.model';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmMergeDialog } from '_components/confirm-merge-dialog/confirm-merge-dialog'
import { ConfirmDeleteDialog } from '_components/confirm-delete-dialog/confirm-delete-dialog';
import { ConfirmSetOwnerDialog } from '_components/confirm-set-owner-dialog/confirm-set-owner-dialog';

@Component({
  selector: 'admin-landlords-page',
  templateUrl: 'admin-landlords.page.html',
  styleUrls: ['admin-landlords.page.scss'],
})
export class AdminLandlordsPage {

  loading: boolean = true;
  landlords: Landlord[];
  page: any;
  breadcrumbs: any;
  searchTerm: string = '';

  @ViewChild('ngFormDirective') formDirective;
  @ViewChild('searchInput') searchInput: ElementRef;
  @ViewChild('cards') cards: ElementRef;
  form: FormGroup;
  submitAttempt: boolean;
  currentlySubmitting: boolean;

  filtersOpen: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private alertService: AlertService,
    private headService: HeadService,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.landlords = [];

    this.page = {
      offset: 0,
      size: 25,
    };

    this.submitAttempt = false;
    this.currentlySubmitting = false;
    this.form = this.formBuilder.group({
      search: [''],
      name: [''],
      phone: [''],
      email: [''],
    });
  }

  ngOnInit() {
    this.headService.setPageTitle('Landlords');
    this.getQueryParamsAndFilter();
    this.getLandlords(this.form.value);
    this.breadcrumbs = [['/admin/dashboard', 'Admin'],'Landlords'];
  }

  ngOnDestroy() {
    this.headService.setPageTitle('');
  }

  pageUpdated($event) {
    this.page.offset = $event.pageIndex;
    this.page.size = $event.pageSize;
  }

  getLandlords(options = {}) {
    this.loading = true;
    this.apiService.getLandlords(options).subscribe(res => {
      this.landlords = res;
      this.loading = false;
    },
    err => {
      console.log('error');
      console.log(err);
      this.loading = false;
    });
  }

  toggleFilters() {
    if (this.filtersOpen === true) {
      this.filtersOpen = false;
    } else {
      this.filtersOpen = true;
      // shift focus to #nameInput
      setTimeout(()=>{ // this will make the execution after the above boolean has changed
        this.searchInput.nativeElement.focus();
      },0);
    }
  }

  resetForm() {
    this.form.reset();
    this.formDirective.resetForm();
    // if we're clearing the form, we're clearing the filters, so we re-submit
    this.submit();
  }

  resetPaginator(){
    this.page.offset = 0;
  }

  submit() {
    this.loading = true;
    this.resetPaginator();
    this.addParamsToUrl()
    this.getSearchTerm();
    this.getLandlords(this.form.value);
    setTimeout(()=>{ // this will make the execution after the above boolean has changed
      this.cards.nativeElement.focus();
    },0);
  }

  getSearchTerm(){
    if(this.form.value.search !== ''){
      this.searchTerm = this.form.value.search;
    }
    else if(this.form.value.name !== ''){
      this.searchTerm = this.form.value.name;
    }
  }

  openMergeLandlordsDialog(landlord: Landlord) {
    let dialogRef = this.dialog.open(ConfirmMergeDialog, { data: { sourceLandlord: landlord } });
    dialogRef.afterClosed().subscribe((res) => {
      if(res === 'true'){
        this.getLandlords();
      }
    })
  }

  openDeleteLandlordDialog(landlord: Landlord){
    let dialogRef = this.dialog.open(ConfirmDeleteDialog, { data: { deleteTargetText: 'Landlord' } });
    dialogRef.afterClosed().subscribe((res) => {
      if (res === 'true') {
        this.deleteLandlord(landlord);
      }
    });  
  }

  
  deleteLandlord(landlord: Landlord){
    this.apiService.deleteLandlord(landlord.id).subscribe(res => {
      this.alertService.success('Landlord Deleted');
      this.getLandlords();
    },

    err => {
      this.alertService.error('Error deleting landlord');
      console.log(err);
    });
  }

  openSetOwnerDialog(landlord: any){
    let dialogRef = this.dialog.open(ConfirmSetOwnerDialog, { data: { ownable: landlord, ownableType: 'landlord' } });
    dialogRef.afterClosed().subscribe((res) => {
      if (res === 'true') {
        this.getLandlords();
      }
    });  
  }
  
  getQueryParamsAndFilter(){
    // If there are any query params, open the filters and pass the values into the form
    if(Object.keys(this.route.snapshot.queryParams).length){
      this.filtersOpen = true;

      this.route.queryParamMap.subscribe(params => {
        this.form.patchValue({
          search: params.get('search'),
          name: params.get('name'),
          phone: params.get('phone'),
          email: params.get('email'),
        });
      });
    }
  }

  addParamsToUrl(){
    let searchTerms: any = {};
    if(this.form.value.search){
      searchTerms.search = this.form.value.search;
    }

      if(this.form.value.name){
        searchTerms.name = this.form.value.name;
      }  
      if(this.form.value.phone){
        searchTerms.phone = this.form.value.phone;
      }
      if(this.form.value.email){
        searchTerms.email = this.form.value.email;
      }
    
    
    this.router.navigate([], {
      queryParams: searchTerms,
    });
  }
}
