import { Component, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService, AlertService } from '_services/index';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged, switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'confirm-merge-dialog',
  templateUrl: './confirm-merge-dialog.html',
  styleUrls: ['./confirm-merge-dialog.scss'],
})
export class ConfirmMergeDialog {
  @ViewChild('ngFormDirective') formDirective;
  form: FormGroup;
  public landlordAutocompletes: any;
  public sourceLandlord: any;
  public possibleDestinationLandlords: any[];


  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { sourceLandlord: any },
    private apiService: ApiService,
    private alertService: AlertService,
    private formBuilder: FormBuilder,
  ) {
    this.sourceLandlord = this.data.sourceLandlord;

    this.form = this.formBuilder.group({
      destinationLandlordName: [''],
    });

    // TODO: if you put in 's' then delete it, result still shows
    this.landlordAutocompletes = this.form.get('destinationLandlordName').valueChanges
      .pipe(
        startWith(''),
        debounceTime(100),
        distinctUntilChanged(),
        switchMap(val => {
          return this.landlordSearch(val || '');
        })
      );
  }

  landlordSearch(value: string): Observable<any[]> {
    if (value.trim() == '') {
      return of([]);
    }
    let observableLL = this.apiService.getLandlords({ search: value });
    // Store all LLs returned in an array
    observableLL.subscribe(res => this.possibleDestinationLandlords = res)
    return observableLL;
  }

  async mergeLandlords() {
    // Find correct destinationLL based on input field value 
    const indexOfDestiationLL = this.possibleDestinationLandlords.findIndex(item => item.name === this.form.value.destinationLandlordName);

    const destinationLandlord = this.possibleDestinationLandlords[indexOfDestiationLL];

    this.apiService.mergeDuplicateLandlords(this.sourceLandlord.id, destinationLandlord.id).subscribe(
      res => {
        this.alertService.success(`Merged ${this.sourceLandlord.name} into ${destinationLandlord.name}`);
      },

      err => {
        this.alertService.error('Error merging landlords');
        console.log(err);
      }
    );
  }
}
