import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AngularMaterialModule } from '_components/angular-material.module';
import { Routes, RouterModule } from '@angular/router';

import { ExternalReviewAddPage } from './external-review-add.page';

const routes: Routes = [
  {
    path: 'external-review/add',
    component: ExternalReviewAddPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AngularMaterialModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ExternalReviewAddPage]
})
export class ExternalReviewAddPageModule {}
