import { Component } from '@angular/core';
import { environment } from '_environment';
// import { APP_CONFIG } from './app.config';
import { HttpClient } from '@angular/common/http';
import { ApiService, AuthenticationService, HeadService } from '_services/index';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public env: any = environment;
  public appMenu: any = false;

  constructor(
    private headService: HeadService,
    private apiService: ApiService,
    public authService: AuthenticationService,
    public httpClient: HttpClient,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.setCustomizations();
    this.initializeApp();
    this.apiService.setUrl(environment.apiURL);
    this.registerIcons();
  }

  setCustomizations() {
    if (this.env.siteName) {
      this.headService.setSiteTitle(this.env.siteName);
    }
    let custom = this.env.custom;
    if (custom.manifestUrl) {
      this.headService.setManifest(custom.manifestUrl);
    }
    if (custom.faviconUrl) {
      this.headService.setFavicon(custom.faviconUrl);
    }
    if (custom.stylesheetUrl) {
      this.headService.setCustomStylesheet(custom.stylesheetUrl);
    }
    if(custom.appMenuUrl) {
      this.httpClient.get<any>(custom.appMenuUrl).subscribe(res=> {
        console.log('menu json', res);
        this.appMenu = res;
      });
    }
  }

  sameOrigin(url) {
    // determines whether the passed in url is of the same origin as the window location
    return url.indexOf(window.location.origin) === 0;
  }

  routerifiedLink(url) {
    // removes the origin of a url
    let origin = new URL(url).origin;
    let link = url.slice(origin.length);
    if (link.length === 0) {
      return '/';
    }
    return link;
  }

  async initializeApp() {
    await this.authService.init();
  }

  async accountManagement() {
    await this.authService.account();
  }

  registerIcons(){
    this.matIconRegistry.addSvgIcon(
      `landlord_icon`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icon/landlord-icon.svg')
    );
    this.matIconRegistry.addSvgIcon(
      `property_icon`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icon/property-icon.svg')
    );
    this.matIconRegistry.addSvgIcon(
      `neighborhood_icon`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icon/neighborhood-icon.svg')
    );
    this.matIconRegistry.addSvgIcon(
      `bed_icon`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icon/bed-icon.svg')
    );
    this.matIconRegistry.addSvgIcon(
      `bath_icon`,
      this.domSanitizer.bypassSecurityTrustResourceUrl('/assets/icon/bath-icon.svg')
    );
  }
}
