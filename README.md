# HousingDB App

This frontend for the HousingDB api is built in Ionic 4, Angular.


# Installation/Docker Commands

It is intended that you will use [docker](https://docs.docker.com/engine/installation/)
and [docker compose](https://docs.docker.com/compose/install/). You'll need to run the
commands below via command line to get started:

---
NOTE: We are going to use a bash alias to make running docker-compose files a bit less verbose. You can run the following to create `docker-compose-local`, `docker-compose-deploy`, and `docker-compose-test` alias commands:
```
echo "alias docker-compose-local='docker-compose --file=docker-compose-local.yml'" >> ~/.bashrc
echo "alias docker-compose-deploy='docker-compose --file=docker-compose-deploy.yml'" >> ~/.bashrc
source ~/.bashrc
echo "alias docker-compose-test='docker-compose --file=docker-compose-test.yml'" >> ~/.bashrc
source ~/.bashrc
```
---

Copy the app config (ready for running locally):
`cp variables.env.example variables.env`

You should edit the variables.env and set the MAPBOX_ACCESS_TOKEN:
`nano variables.env`

Compile the initial image, or if the Dockerfile changes:
`docker-compose build app`

Bring up the container. This will tie the running process and logs to your terminal:
`docker-compose up app`

You can now view the app in your browser:
[http://localhost:4201](http://localhost:4201)

To instead run a container detached, you can run the following:
`docker-compose up -d app`

To view a detached container's logs as they are generated:
`docker-compose logs --follow`

To stop a detached container:
`docker-compose stop`

To open a bash shell in a container:
`docker-compose exec bash app`

# Configuration
For feedback and owner requests, you will need to pass a url into the formUrl in the environment.ts file. That url can be constructed with the Google form's url followed by queries to the specific form field's entry numbers. The value of each entry query should be copied from the example environments, and enlosed in []. Ex: [PAGE_URL]

The entry numbers (entry.123456789) are the id to an input on the form, so it can be automatically filled in. 
You can get the entry numbers from the form by inspecting the HTML element containing the desired input (Other methods can be found online).
In the highest level element of the form field container, find the 'data-params' property. The EntryId is the second 9-digit sequence found in the nested array.

A constructed url should look something like:

https://docs.google.com/forms/d/e/XXXXXXXXXXXXXXXXXXXXX/viewform?entry.1234567890=[PAGE_URL]\&\&entry.0987654321=[CONTENT_SECTION]
https://docs.google.com/forms/d/e/XXXXXXXXXXXXXXXXXXXXX/viewform?entry.1238160640=[CLAIMED_CONTENT]\&\&entry.664246140=[USER_ID]

Important: You must escape the ampersands with "\" so the evironment.ts properties get copied correctly.

# Committing

Good guide for git commits:
https://chris.beams.io/posts/git-commit/

