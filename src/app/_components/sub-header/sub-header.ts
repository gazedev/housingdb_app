import { Input, Component } from '@angular/core';
import { Router } from '@angular/router'
import { AuthenticationService, ApiService } from '_services/index';
import { environment } from '_environment';

@Component({
  selector: 'sub-header',
  templateUrl: './sub-header.html',
  styleUrls: ['./sub-header.scss'],
})
export class SubHeaderComponent {

  @Input('matIcon')
  public matIcon: string;
  
  @Input('svgIcon')
  public svgIcon: string;
  
  @Input('heading')
  public heading: string;
  
  @Input('colorType')
  colorType: string;
  
  @Input('showClaimOwner')
  showClaimOwner: boolean;

  color: string;
  isMatIcon: boolean;

  constructor(
    private router: Router,
    public authService: AuthenticationService,
    public apiService: ApiService,
  ) {

  }
  ngOnInit(){
    this.setBackgroundColor();
    if(this.matIcon){
      this.isMatIcon = true;
    }
  }

  GiveFeedback(){
    const unprocessedUrl = environment.formUrl.feedback;
    
    const routeUrl = window.location.origin + this.router.url; 
    const processedUrl = unprocessedUrl.replace('[PAGE_URL]', routeUrl ).replace('[CONTENT_SECTION]', this.heading.replace(/ *\([^)]*\) */g, ""));

    
    window.location.href = processedUrl;
  }

  ClaimContent(){
    // Will this be an issue if the api call fails, then the link to the request form will not work?
    this.apiService.getAccount().subscribe(res => {
      const userId = res.id;      
      const unprocessedUrl = environment.formUrl.owner;
      const routeUrl = window.location.origin + this.router.url; 
      const processedUrl = unprocessedUrl.replace('[CLAIMED_CONTENT]', routeUrl ).replace('[USER_ID]', userId);

      window.location.href = processedUrl;
      },
      err => {
        console.log('Error getting account', err); 
      }
    );

  }

  setBackgroundColor(){
    switch(this.colorType){
      case 'card':
        this.color = '#f1f1f1';
        break;
      case 'property':
        this.color = '#dbeee2';
        break;
      case 'landlord':
        this.color = '#def2fc';
        break;
      case 'review':
        this.color = '#fefadc';
        break;
      default:          
        break;
      }
      // From 'src/colors.scss', we probably want to figure out a less hard coded way to do this, so changing colors.scss automatically changes this as well.
      // $property-color: #dbeee2;
      // $landlord-color: #def2fc;
      // $review-color: #fefadc;
      // $card-gray: #f1f1f1;
      // $border-color: #d0d0d0;
      // $admin-color: #fedcdc;
      // $profile-color: #fedcfd;
  }
}
