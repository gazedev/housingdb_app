import { Input, Component, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContentService } from '_services/index';
import { AuthenticationService } from '_services/authentication.service'
import { ApiService } from '_services/api.service'

@Component({
  selector: 'review-card',
  templateUrl: 'review-card.html',
  styleUrls: ['review-card.scss'],
})
export class ReviewCardComponent {

  @Input('review')
  public review: any;
  public isTarget: boolean;
  public routeToReview: string;

  constructor(
    private route: ActivatedRoute,
    private elementRef: ElementRef,
    public content: ContentService,
    public authService: AuthenticationService,
    private apiService: ApiService,

  ) {
    this.isTarget = false;

  }

  ngOnInit() {
    this.getRouteToReviewedItem();
    this.route.fragment.subscribe(fragments => {
      if (fragments == this.review.id) {
        this.isTarget = true;
        this.elementRef.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
      } else {
        this.isTarget = false;
      }
    });
  }

  getRouteToReviewedItem() {
    let reviewableMachineName;
    if (this.review.reviewableType === "landlord") {
      this.apiService.getLandlord(this.review.reviewableId).subscribe( res => {
        reviewableMachineName = res.machineName;
        this.routeToReview = `/landlord/${reviewableMachineName}`;
      },
      err => {
        console.log('error getting landlord', err)
      });
    } 
    else {
      this.apiService.getProperty(this.review.reviewableId).subscribe(res => {
        reviewableMachineName = res.machineName;
        this.routeToReview = `/property/${reviewableMachineName}`;
      },
        err => {
          console.log('error getting property', err);
      });
    }
  }
}
