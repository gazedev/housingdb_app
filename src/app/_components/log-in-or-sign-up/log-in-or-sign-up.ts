import { Input, Component } from '@angular/core';
import { AuthenticationService } from '_services/authentication.service';

@Component({
  selector: 'log-in-or-sign-up',
  templateUrl: './log-in-or-sign-up.html',
  styleUrls: ['./log-in-or-sign-up.scss'],
})
export class LogInOrSignUpComponent {
  @Input('buttonColor') 
  buttonColor: string;
  likelyHasAccount: boolean;
  
  constructor(
    public authService: AuthenticationService,
  ) {

  }

  ngOnInit() {
    if(JSON.parse(localStorage.getItem('likelyHasAccount'))){
      this.likelyHasAccount = true;
    }
    else
    {
      this.likelyHasAccount = false;
    }
  }

  async doLoginOrSignUp() {
    await this.authService.logInOrSignUp();
  }

  async doLogout() {
    await this.authService.logout();
  }
}
