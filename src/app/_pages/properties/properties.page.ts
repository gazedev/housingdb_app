import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService, HeadService } from '_services/index';
import { Property } from '_models/property.model';

@Component({
  selector: 'properties-page',
  templateUrl: 'properties.page.html',
  styleUrls: ['properties.page.scss'],
})
export class PropertiesPage {

  loading: boolean = true;
  properties: Property[];
  page: any;
  landlords: any;
  locations: any;
  locationNames: any;
  allLocations: any;
  breadcrumbs: any;

  @ViewChild('ngFormDirective') formDirective;
  @ViewChild('nameInput') nameInput: ElementRef;
  @ViewChild('cards') cards: ElementRef;
  form: FormGroup;
  submitAttempt: boolean;
  currentlySubmitting: boolean;

  filtersOpen: boolean = false;
  originalOrder: any;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private headService: HeadService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.properties = [];
    this.landlords = {};
    this.locations = {};
    this.locationNames = {};
    this.allLocations = {};

    this.page = {
      offset: 0,
      size: 25,
    };

    this.submitAttempt = false;
    this.currentlySubmitting = false;
    this.form = this.formBuilder.group({
      name: [''],
      address: [''],
      bedrooms: [''],
      locations: [''],
    });

    // used to preserve order in the keyvalue pipe in the template
    this.originalOrder = (a: any, b: any): number => {
      return 0;
    }
  }

  ngOnInit() {
    this.breadcrumbs = ['Properties'];
    this.loadAllLocations();
    this.getProperties(this.form.value);
    this.headService.setPageTitle('Properties');
  }

  ngOnDestroy() {
    this.headService.setPageTitle('');
  }

  pageUpdated($event) {
    this.page.offset = $event.pageIndex;
    this.page.size = $event.pageSize;
  }

  getProperties(options = {}) {
    this.loading = true;
    this.apiService.getProperties(options).subscribe(res => {
      this.properties = res;
      this.loadPropertiesLandlords();
      this.loadPropertiesLocations();
      this.loading = false;
    },
    err => {
      console.log('Error getting Properties', err);
      this.loading = false;
    });
  }

  extractAddress(property: any) {
    let addr = property.PostalAddresses[0];
    return `${addr.streetAddress}, ${addr.addressLocality}, ${addr.addressRegion} ${addr.postalCode}`;
  }

  extractNeighborhood(property: any) {
    let addr = property.PostalAddresses[0];
    return addr.addressNeighborhood;
  }

  loadPropertiesLandlords() {
    for (let property of this.properties) {
      if (property.LandlordId && !this.landlords.hasOwnProperty(property.LandlordId)) {
        this.loadLandlord(property);
      }
    }
  }

  loadLandlord(property: any) {
    // set a placeholder so we know not to load it again
    this.landlords[property.LandlordId] = true;
    this.apiService.getLandlord(property.LandlordId).subscribe(
    res => {
      // create a keyed array so we only have to load each landlord once
      // and can access it in O(1).
      this.landlords[property.LandlordId] = res;
    },
    err => {
      console.log('Error loading Landlord', err);
    });
  }

  loadPropertiesLocations() {
    for (let property of this.properties) {
      if (property.LocationId && !this.locations.hasOwnProperty(property.LocationId)) {
        this.loadLocation(property);
      }
    }
  }

  loadLocation(property: any) {
    // set a placeholder so we know not to load it again
    this.locations[property.LocationId] = true;
    this.apiService.getLocation(property.LocationId).subscribe(
    res => {
      // create a keyed array so we only have to load each landlord once
      // and can access it in O(1).
      this.locations[property.LocationId] = res;
    },
    err => {
      console.log('Error loading location', err);
    });
  }

  loadAllLocations() {
    this.apiService.getLocations().subscribe(res => {
      this.buildAllLocations(res);
    });
  }

  buildAllLocations(locations) {
    for (let state of locations) {
      for (let city of state.children) {
        if (city.children.length === 1 && city.name === city.children[0].name) {
          this.allLocations[city.children[0].id] = {
            name: city.children[0].name,
          };
        } else {
          this.allLocations[city.id] = {
            name: city.name,
            children: {},
          };
          for (let location of city.children) {
            this.allLocations[city.id].children[location.id] = {
              name: location.name,
            };
            // Fill locationNames object with key of location name and value of location id 
            this.locationNames[location.name] = location.id;
          }
        }
      }
    }
    // After building locationNames object we can get query params
    // which depends on having the location names 
    this.getQueryParamsAndFilter();
  }

  toggleFilters() {
    if (this.filtersOpen === true) {
      this.filtersOpen = false;
    } else {
      this.filtersOpen = true;
      // shift focus to #nameInput
      setTimeout(()=>{ // this will make the execution after the above boolean has changed
        this.nameInput.nativeElement.focus();
      },0);
    }
  }

  resetForm() {
    this.form.reset();
    this.formDirective.resetForm();
    // if we're clearing the form, we're clearing the filters, so we re-submit
    this.submit();
  }

  resetPaginator(){
    this.page.offset = 0;
  }
  
  submit() {
    console.log(this.form.value);
    this.loading = true;
    this.resetPaginator();
    this.addParamsToUrl();
    this.getProperties(this.form.value);

    // This only works if you click submit button... 
    // pressing 'Enter' to submit doesn't change the focus.
    setTimeout(()=>{ // this will make the execution after the above boolean has changed
      this.cards.nativeElement.focus();
    },0);
  }

  getQueryParamsAndFilter(){
    // If there are any query params, open the filters and pass the values into the form
    if(Object.keys(this.route.snapshot.queryParams).length){
      this.filtersOpen = true;

      this.route.queryParamMap.subscribe(params => {
        // Convert the names in location query params back to ids
        const locationNamesInParams = params.getAll('neighborhood');
        let locationIds = [];

        if(locationNamesInParams.length > 0){
          for(let name of locationNamesInParams){
            locationIds.push(this.locationNames[name]);
          }
        }

        this.form.patchValue({
          name: params.get('name'),
          address: params.get('address'),
          bedrooms: params.get('bedrooms'),
          locations: locationIds,
        });
        
        // Call Get properties to apply form changes to page
        this.getProperties(this.form.value);
      });
    };    
  };

  addParamsToUrl(){
    let searchTerms: any = {};
    if(this.form.value.name){
      searchTerms.name = this.form.value.name;
    }
    if(this.form.value.address){
      searchTerms.address = this.form.value.address;
    }  
    if(this.form.value.bedrooms){
      searchTerms.bedrooms = this.form.value.bedrooms;
    }
    if(this.form.value.locations){
      // Convert the location values of the query params to be names rather than id
      let neighborhoods = [];
      for(let location of this.form.value.locations){
        neighborhoods.push(this.locations[location].name)
      }
      searchTerms.neighborhood = neighborhoods;
    }
    
    this.router.navigate([], {
      queryParams: searchTerms,
    });
  };

  compareFn(option: any, paramValue: any): boolean {
    // Ensure default (-Select-) if no param is given
    if(paramValue){
      return option == paramValue;
    }    
    return false;
  };
}
