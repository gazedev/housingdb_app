import { Component, OnInit } from '@angular/core';
import { AlertService, ApiService } from '_services/index';
import { ReviewCardComponent } from '_components/review-card/review-card';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDeleteDialog } from '_components/confirm-delete-dialog/confirm-delete-dialog';

@Component({
  selector: 'admin-reviews-page',
  templateUrl: './admin-reviews.page.html',
  styleUrls: ['./admin-reviews.page.scss'],
})
export class AdminReviewsPage implements OnInit {
  loading: boolean = true;
  reviewList: [];
  breadcrumbs: any;
  constructor(
    private apiService: ApiService,
    private alertService: AlertService,
    private dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.loadReviews();
    this.breadcrumbs = [['/admin/dashboard', 'Admin' ],'Reviews'];
  }

  loadReviews() {
    this.loading = true;
    this.apiService.getAllReviews().subscribe(
      (res) => {
        this.reviewList = res;
        this.loading = false;
      },
      (err) => {
        console.log('Error getting Reviews', err);
        this.loading = false;
      }
    );
  }

  openDeleteReviewDialog(review: any) {
    let dialogRef = this.dialog.open(ConfirmDeleteDialog, { data: { deleteTargetText: 'Review' } });
    dialogRef.afterClosed().subscribe((res) => {
      if (res === 'true') {
        this.deleteReview(review);
      }
    });
  }

  deleteReview(review) {
    this.apiService.deleteReview(review.id).subscribe(
      res => {
        this.alertService.success('Review Deleted')
        this.loadReviews();
      },

      err => {
        this.alertService.error('Error deleting review');
        console.log(err);
      }
    );
  }
}
