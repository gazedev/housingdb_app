import { Component, OnInit, ViewChild } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '_services/api.service';
import { AlertService } from '_services/alert.service';
import { AuthenticationService } from '_services/index';
import { NumberRangeValidator, NumberRangeItemValidator } from '_validators/range-validator';
import { UrlValidator } from '_validators/url-validator';
import { emptyish } from '_helpers/emptyish';
import { Observable, of } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged, switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-property-add',
  templateUrl: './property-add.page.html',
  styleUrls: ['./property-add.page.scss'],
})
export class PropertyAddPage implements OnInit {

  @ViewChild('ngFormDirective') formDirective;
  form: FormGroup;
  submitAttempt: boolean;
  currentlySubmitting: boolean;
  landlordAutocompletes: any;
  addressAutocompletes: any;
  breadcrumbs: any;
  isReviewing: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService,
    public authService: AuthenticationService,
  ) {
    this.submitAttempt = false;
    this.currentlySubmitting = false;
    this.form = this.formBuilder.group({
      address: ['', Validators.compose([Validators.required])],
      landlordQuickInfo: [''],
      claimOwnership: [false],
      name: [''],
      bedrooms: this.formBuilder.group({
        min: ['', this.bedroomItemValidator()],
        max: ['', this.bedroomItemValidator()],
      }, {
        validator: NumberRangeValidator
      }),
      bathrooms: this.formBuilder.group({
        min: ['', this.bathroomItemValidator()],
        max: ['', this.bathroomItemValidator()],
      }, {
        validator: NumberRangeValidator
      }),
      website: ['', UrlValidator],
      contact: [''],
      body: [''],
    });
    // TODO: if you put in 's' then delete it, result still shows
    this.landlordAutocompletes = this.form.get('landlordQuickInfo').valueChanges
      .pipe(
        startWith(''),
        debounceTime(100),
        distinctUntilChanged(),
        switchMap(val => {          
          return this.landlordSearch(val || '');
        })
      );

    this.addressAutocompletes = this.form.get('address').valueChanges
      .pipe(
        startWith(''),
        debounceTime(100),
        distinctUntilChanged(),
        switchMap(val => {
          return this.addressSearch(val || '');
        }),
        // tap({
        //   next: val => {console.log(val)},
        // })
      );
  }

  async ngOnInit() {
    this.breadcrumbs = [['/properties', 'Properties'], 'Add'];
    await this.authService.checkLogin();

    this.route.queryParamMap.subscribe(params => {
      let autofillLandlord = params.get('landlord')
      this.getLandlordFromParams(autofillLandlord);
      
      let intent = params.get('intent');
      if(intent === 'review'){
        this.isReviewing = true;
      }
    });
  }

  landlordSearch(value: string): Observable<any[]> {
    if (value.trim() == '') {
      return of([]);
    }    
    return this.apiService.getLandlords({search: value});
  }

  addressSearch(value: string): Observable<any[]> {
    if (value.trim() == '') {
      return of([]);
    }
    
    return this.apiService.getAddressGeocode(value)
    .pipe(
      map(res => res.results)
    );
  }

  constructAddress(component: any) {
    return `${component.house_number} ${component.road}, ${component.city}, ${component.state_code} ${component.postcode}`;
  }

  bedroomItemValidator() {
    return NumberRangeItemValidator({
      modulo: 1,
      min: 0,
      max: 10,
    });
  }

  bedroomValueDuplicate() {
    let minControl = this.form.get('bedrooms.min');
    let maxControl = this.form.get('bedrooms.max');
    if (minControl.value === '') {
      minControl.setValue(maxControl.value);
    } else if (maxControl.value === '') {
      maxControl.setValue(minControl.value);
    }
  }

  bedroomValuesSwitch() {
    let minControl = this.form.get('bedrooms.min');
    let maxControl = this.form.get('bedrooms.max');
    let minValue = minControl.value;
    minControl.setValue(maxControl.value);
    maxControl.setValue(minValue);
  }

  bathroomItemValidator() {
    return NumberRangeItemValidator({
      modulo: .5,
      min: 1,
      max: 9,
    });
  }

  bathroomValueDuplicate() {
    let minControl = this.form.get('bathrooms.min');
    let maxControl = this.form.get('bathrooms.max');
    if (minControl.value === '') {
      minControl.setValue(maxControl.value);
    } else if (maxControl.value === '') {
      maxControl.setValue(minControl.value);
    }
  }

  bathroomValuesSwitch() {
    let minControl = this.form.get('bathrooms.min');
    let maxControl = this.form.get('bathrooms.max');
    let minValue = minControl.value;
    minControl.setValue(maxControl.value);
    maxControl.setValue(minValue);
  }

  resetForm() {
    this.form.reset();
    this.formDirective.resetForm();
  }

  submit() {
    this.currentlySubmitting = true;
    this.submitAttempt = true;

    if (!this.form.valid) {
      console.log('form invalid!');
      console.log(this.form);
      return;
    }

    this.alertService.success('Submitting...');

    let formValues = this.form.value;
    let landlord: any = {};
    let landlordQuickInfo: boolean = false;

    if (
      formValues.landlordQuickInfo !== '' &&
      formValues.landlordQuickInfo !== null
    ) {
      landlord.quickInfo = formValues.landlordQuickInfo;
      landlordQuickInfo = true;
    }

    let property = this.propertyMapLocalToApi(formValues);

    if (formValues.claimOwnership === true) {
      this.apiService.getAccount().subscribe(
        response => {
          property.AuthorId = response.id;
          if (landlordQuickInfo) {
            this.addProperty(property, landlord);
          } else {
            this.addProperty(property);
          }
        },
      );
    } else {
      if (landlordQuickInfo) {
        this.addProperty(property, landlord);
      } else {
        this.addProperty(property);
      }
    }
  }

  propertyMapLocalToApi(formValues) {
    let property: any = {};
    // let landlord: any = {};
    for (var key in formValues) {
      console.log(key, formValues[key]);
      if (
        emptyish(formValues[key])
      ) {
        continue;
      }

      switch (key) {
        case 'claimOwnership':
          // we'll manually check this later and look up account info if checked
          // but we do need to prevent it from being added to property as is
          break;
        case 'landlordQuickInfo':
          // we don't want to to be added to the property object
          break;
        case 'bedrooms':
          if (
            !emptyish(formValues.bedrooms.min)
          ) {
            property.bedroomsMin = formValues.bedrooms.min;
            property.bedroomsMax = formValues.bedrooms.max;
          }
          break;
        case 'bathrooms':
          if (
            !emptyish(formValues.bathrooms.min)
          ) {
            property.bathroomsMin = formValues.bathrooms.min;
            property.bathroomsMax = formValues.bathrooms.max;
          }
          break;
        default:
          property[key] = formValues[key];
      }
    }
    return property;
  }

  addProperty(property, landlord: any = undefined) {
    this.apiService.addProperty(property).subscribe(
      propertyResponse => {
        let propertyId = propertyResponse.id;
        if (landlord !== undefined) {
          if(!this.isReviewing){
            this.displayPropertyCreatedToast(propertyId);
          }
          this.addPropertyLandlord(propertyId, landlord);
        } else { // no landlord info, we're done
          if(this.isReviewing){
            this.router.navigate(['./reviews/add'], {queryParams: {reviewableType: 'property', reviewableId: propertyId}})
          }
          else{
            this.displayPropertyCreatedToast(propertyId);
            this.resetForm();
          }
        }
      },
      propertyErrorResponse => {
        let propertyId = propertyErrorResponse.headers.get('Content-Location');
        if (propertyErrorResponse.status === 422 && propertyId) {
          // Property already exists and we should let the user know
          if (landlord !== undefined) {
            this.addPropertyLandlord(propertyId, landlord);
          } else { // no landlord info, we're done
            if(this.isReviewing){
              this.router.navigate(['./reviews/add'], {queryParams: {reviewableType: 'property', reviewableId: propertyId}})
            }
            else{
              this.displayPropertyExistsToast(propertyId);
              this.resetForm();
            }
          }
        } else {
          console.log('Error adding the property. Not 422, or no contentLocation', propertyErrorResponse);
          this.alertService.error('Error creating property with this address. Did you enter the correct address?')
        }
      }
    );
  }

  addPropertyLandlord(propertyId, landlord) {
    this.apiService.addLandlord(landlord).subscribe(
      landlordRequestResponse => {
        let landlordResponse = landlordRequestResponse.body;
        this.apiService.addLandlordToProperty(propertyId, landlordResponse.id).subscribe(updateResponse => {
          if(this.isReviewing){
            this.router.navigate(['./reviews/add'], {queryParams: {reviewableType: 'property', reviewableId: propertyId}})
          }
          else{
            this.displayPropertyUpdatedToast(propertyId);
            this.resetForm();
          }
        });
      },
      landlordErrorResponse => {
        let contentLocation = landlordErrorResponse.headers.get('Content-Location');
        if (landlordErrorResponse.status === 422 && contentLocation) {
          // landlord already exists and we should use that id to attach to our property
          this.apiService.addLandlordToProperty(propertyId, contentLocation).subscribe(updateResponse => {
            if(this.isReviewing){
              this.router.navigate(['./reviews/add'], {queryParams: {reviewableType: 'property', reviewableId: propertyId}})
            }
            else{
              this.displayPropertyUpdatedToast(propertyId);
              this.resetForm();
            }
          });
        }
      }
    );
  }

  getLandlordFromParams(landlordMachineName: string){
    if(landlordMachineName){
      this.apiService.getLandlordByMachineName(landlordMachineName).subscribe(res=> {
        this.form.patchValue({landlordQuickInfo: res.name});
      }, err => {
        console.log(err);
      });
    }
  }

  displayPropertyCreatedToast(propertyId) {
    this.alertService.action({
      data: {
        message: 'The property has been created.',
        action: {
          text: 'View Property',
          navigateTo: `/property/${propertyId}`,
        },
      }
    });
  }

  displayPropertyUpdatedToast(propertyId) {
    this.alertService.action({
      data: {
        message: 'The property has been updated with the landlord.',
        action: {
          text: 'View Property',
          navigateTo: `/property/${propertyId}`,
        },
      }
    });
  }

  displayPropertyExistsToast(propertyId) {
    this.alertService.action({
      data: {
        message: 'It looks like we already have a property with that address.',
        action: {
          text: 'View Property',
          navigateTo: `/property/${propertyId}`,
        },
      }
    });
  }

}
