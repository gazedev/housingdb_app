import { Inject, Component } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'confirm-delete-dialog',
  templateUrl: './confirm-delete-dialog.html',
  styleUrls: ['./confirm-delete-dialog.scss'],
})
export class ConfirmDeleteDialog {
  
  public deleteTargetText: string;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { deleteTargetText: string },
    ) {
    this.deleteTargetText = data.deleteTargetText;
  }
}
