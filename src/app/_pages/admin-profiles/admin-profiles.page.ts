import { Component, OnInit } from '@angular/core';
import { ApiService } from '_services/index';

@Component({
  selector: 'admin-profiles-page',
  templateUrl: './admin-profiles.page.html',
  styleUrls: ['./admin-profiles.page.scss'],
})
export class AdminProfilesPage implements OnInit {
  loading: boolean = true;
  profileList: [];
  breadcrumbs: any;
  constructor(
    private apiService: ApiService,
  ) {}

  ngOnInit() {
    this.breadcrumbs = [['/admin/dashboard', 'Admin'], 'Profiles'];
    this.loadProfiles();
  }

  loadProfiles() {
    this.loading = true;
    this.apiService.getAccounts().subscribe(
      (res) => {
        this.profileList = res;
        this.loading = false;
        console.log(this.profileList);
        
      },
      (err) => {
        console.log('Error getting Profiles/Accounts', err);
        this.loading = false;
      }
    );
  }
}
