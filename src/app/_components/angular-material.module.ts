import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
} from '@angular/material';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';


import { ActionSnackBarComponent } from './action-snack-bar/action-snack-bar';
import { PropertyCardComponent } from './property-card/property-card';
import { LandlordCardComponent } from './landlord-card/landlord-card';
import { LoadingBlockComponent } from './loading-block/loading-block';
import { NoContentComponent } from './no-content/no-content';
import { MapBoxComponent } from './map-box/map-box';
import { ReviewCardComponent } from './review-card/review-card';
import { StarsDisplayComponent } from './stars-display/stars-display';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs';
import { LogInOrSignUpComponent } from './log-in-or-sign-up/log-in-or-sign-up';
import { SubHeaderComponent } from './sub-header/sub-header';

@NgModule({
  declarations: [
    ActionSnackBarComponent,
    PropertyCardComponent,
    LandlordCardComponent,
    LoadingBlockComponent,
    MapBoxComponent,
    NoContentComponent,
    ReviewCardComponent,
    StarsDisplayComponent,
    BreadcrumbsComponent,
    LogInOrSignUpComponent,
    SubHeaderComponent,
  ],
  imports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: 'property-card',
        component: PropertyCardComponent,
      },
      {
        path: 'landlord-card',
        component: LandlordCardComponent,
      },
      {
        path: 'loading-block',
        component: LoadingBlockComponent,
      },
      {
        path: 'no-content',
        component: NoContentComponent,
      },
      {
        path: 'review-card',
        component: ReviewCardComponent,
      },
      {
        path: 'breadcrumbs',
        component: BreadcrumbsComponent,
      },
    ])
  ],
  exports: [
    MatAutocompleteModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatCardModule,
    MatDividerModule,
    MatExpansionModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatPaginatorModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    ActionSnackBarComponent,
    PropertyCardComponent,
    LandlordCardComponent,
    LoadingBlockComponent,
    MapBoxComponent,
    NoContentComponent,
    ReviewCardComponent,
    StarsDisplayComponent,
    BreadcrumbsComponent,
    LogInOrSignUpComponent,
    SubHeaderComponent,
  ],
})
export class AngularMaterialModule {}
