import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService, AuthenticationService, ContentService, HeadService } from '_services/index';
import { Property } from '_models/property.model';
import { switchMap } from 'rxjs/operators';
import { Landlord } from '_models/landlord.model';
import * as MarkdownIt from 'markdown-it';

@Component({
  selector: 'app-property-detail',
  templateUrl: './property-detail.page.html',
  styleUrls: ['./property-detail.page.scss'],
})
export class PropertyDetailPage implements OnInit {

  public userAccount: any;
  public propertyId: string;
  // public property: Property;
  public property: any;
  public landlordId: string;
  public landlord: any;
  public reviews: any;
  public externalReviews: any;
  public breadcrumbs: any;
  public address: string;
  public street: string;
  public cityStateZip: string;
  public location: any;
  public bedrooms: {text: string, speech: string};
  public bathrooms: {text: string, speech: string};


  constructor(
    private route: ActivatedRoute,
    public content: ContentService,
    private apiService: ApiService,
    public authenticationService: AuthenticationService,
    public headService: HeadService,
  ) {
    this.property = <Property>{};
    this.landlord = {};
    this.reviews = [];
    this.bedrooms = {text: '?', speech: 'Unknown number of'};
    this.bathrooms = {text: '?', speech: 'Unknown number of'};
  }

  async ngOnInit() {
    await this.authenticationService.checkLogin();
    if (this.authenticationService.isAuthenticated) {
      this.getAccount();
    }

    this.route.paramMap.subscribe(params => {
      if (!this.isUuid(params.get('id'))) {
        this.apiService.getPropertyByMachineName(params.get('id')).subscribe(res => {
          this.propertyId = res.id;
          this.getPropertyAndRelatedContent();
        });
      } else {
        this.propertyId = params.get('id');
        this.getPropertyAndRelatedContent();
      }
    });
  }

  ngOnDestroy() {
    this.headService.setPageTitle('');
  }

  isUuid(input) {
    const regex = RegExp(/^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i);
    return regex.test(input);
  }

  getAccount() {
    this.apiService.getAccount().subscribe(res => {
      this.userAccount = res;
    });
  }

  getPropertyAndRelatedContent() {
    this.getPropertyReviews();
    this.getPropertyExternalReviews();
    this.apiService.getProperty(this.propertyId).subscribe(res => {
      this.property = res;

      // Display Commonmark for body
      if(this.property.body){
        const md = new MarkdownIt('commonmark').disable('image');
        this.property.body = md.render(this.property.body);

      }

      this.getBedBathDisplay();
      this.extractAddress();
      this.loadLocation();
      this.breadcrumbs = [['/properties', 'Properties'], this.property.name];
      this.headService.setPageTitle(this.property.name);
      if (this.property.LandlordId) {
        this.landlordId = this.property.LandlordId;
        this.loadPropertyLandlord();
      }
    },
    err => {
      console.log('error');
      console.log(err);
    });
  }

  loadPropertyLandlord() {
    this.apiService.getLandlord(this.landlordId).subscribe(res => {
      this.landlord = res;
    },
    err => {
      console.log('error');
      console.log(err);
    });
  }

  extractNeighborhood(property: any) {
    let addr = property.PostalAddresses[0];
    return addr.addressNeighborhood;
  }

  extractAddress() {
    if (this.property.PostalAddresses) {
      let addr = this.property.PostalAddresses[0];
      this.address = `${addr.streetAddress}, ${addr.addressLocality}, ${addr.addressRegion} ${addr.postalCode}`;
      this.street = `${addr.streetAddress}` 
      this.cityStateZip = `${addr.addressLocality}, ${addr.addressRegion} ${addr.postalCode}`;
    } else {
      this.address = '';
    }
  }

  getPropertyReviews() {
    this.apiService.getPropertyReviews(this.propertyId).subscribe(res => {
      this.reviews = res;
    },
    err => {
      console.log('error getting property reviews', err);
    });
  }

  getPropertyExternalReviews() {
    this.apiService.getPropertyExternalReviews(this.propertyId).subscribe(res => {
      this.externalReviews = res.map(x => {
        let hostname = new URL(x.url).hostname;
        hostname = hostname.replace(/^www\./i, '');
        hostname = hostname.charAt(0).toUpperCase() + hostname.slice(1);
        x.name = `Review on ${hostname}`;
        return x;
      });
    },
    err => {
      console.log('error getting property external reviews', err);
    });
  }

  loadLocation() {
    this.apiService.getLocation(this.property.LocationId).subscribe(res => {
      this.location = res;
    })
  }

  getBedBathDisplay(){
    // Get Beds for Display:
    if(this.property.bedroomsMin != this.property.bedroomsMax ){
      this.bedrooms.text = `${this.property.bedroomsMin.toString()}-${this.property.bedroomsMax.toString()}`;
      this.bedrooms.speech = `Number of bedrooms: ${this.property.bedroomsMin.toString()} to ${this.property.bedroomsMax.toString()} Bedrooms.`;
    } 
    else if (this.property.bedroomsMin == this.property.bedroomsMax && this.property.bedroomsMin != null) {
      this.bedrooms.text = this.property.bedroomsMin.toString();
      this.bedrooms.speech = `Number of bedrooms: ${this.bedrooms.text} Bedrooms.`;
    }
    else {
      this.bedrooms.text = '?';
      this.bedrooms.speech = 'Unknown number of Bedrooms.';
    }
    // Get Baths for Display:
    if(this.property.bathroomsMin != this.property.bathroomsMax ){
      this.bathrooms.text = `${this.property.bathroomsMin.toString()}-${this.property.bathroomsMax.toString()}`;
      this.bathrooms.speech = `Number of bathrooms: ${this.property.bathroomsMin.toString()} to ${this.property.bathroomsMax.toString()} Bathrooms.`;
    } 
    else if (this.property.bathroomsMin == this.property.bathroomsMax && this.property.bathroomsMin != null) {
      this.bathrooms.text = this.property.bathroomsMin.toString();
      this.bathrooms.speech = `Number of bathrooms: ${this.bathrooms.text} Bathrooms.`;
    }
    else {
      this.bathrooms.text = '?';
      this.bathrooms.speech = 'Unknown number of Bathrooms.';
    }
  }
}
