import { Input, Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '_services/api.service';

@Component({
  selector: 'property-card',
  templateUrl: './property-card.html',
  styleUrls: ['./property-card.scss'],
})
export class PropertyCardComponent {
  @Input('property')
  public property: any;

  @Input('landlord')
  public landlord: any;

  @Input('location')
  public location: any;

  public address: string;
  public street: string;
  public cityStateZip: string;
  public bedrooms: {text: string, speech: string};
  public bathrooms: {text: string, speech: string};
  constructor (
    private router: Router,
    private apiService: ApiService,
  ) {
    this.bedrooms = {text: '?', speech: 'Unknown number of'};
    this.bathrooms = {text: '?', speech: 'Unknown number of'};
  }

  ngOnInit () {
    this.extractAddress();
    this.getBedBathDisplay();
  }
  
  ngOnChanges(changes: any) {
    // If the property has a Location and it wasn't loaded for us,
    // load it ourselves
    if (changes.hasOwnProperty('property')) {
      if (this.property.LocationId && !this.location) {
        this.loadLocation();
      }
    }
  }

  extractAddress() {  
    if (this.property.PostalAddresses) {
      let addr = this.property.PostalAddresses[0];
      this.address = `${addr.streetAddress}, ${addr.addressLocality}, ${addr.addressRegion} ${addr.postalCode}`;
      this.street = `${addr.streetAddress}`;
      this.cityStateZip = `${addr.addressLocality}, ${addr.addressRegion} ${addr.postalCode}`;
    } else {
      this.address = '';
    }
  }

  loadLocation() {
    this.apiService.getLocation(this.property.LocationId).subscribe(res => {
      this.location = res;
    })
  }
  

  getBedBathDisplay(){
    // Get Beds for Display:
    if(this.property.bedroomsMin != this.property.bedroomsMax ){
      this.bedrooms.text = `${this.property.bedroomsMin.toString()}-${this.property.bedroomsMax.toString()}`;
      this.bedrooms.speech = `Number of bedrooms: ${this.property.bedroomsMin.toString()} to ${this.property.bedroomsMax.toString()} Bedrooms.`;
    } 
    else if (this.property.bedroomsMin == this.property.bedroomsMax && this.property.bedroomsMin != null) {
      this.bedrooms.text = this.property.bedroomsMin.toString();
      this.bedrooms.speech = `Number of bedrooms: ${this.bedrooms.text} Bedrooms.`;
    }
    else {
      this.bedrooms.text = '?';
      this.bedrooms.speech = 'Unknown number of Bedrooms.';
    }
    // Get Baths for Display:
    if(this.property.bathroomsMin != this.property.bathroomsMax ){
      this.bathrooms.text = `${this.property.bathroomsMin.toString()}-${this.property.bathroomsMax.toString()}`;
      this.bathrooms.speech = `Number of bathrooms: ${this.property.bathroomsMin.toString()} to ${this.property.bathroomsMax.toString()} Bathrooms.`;
    } 
    else if (this.property.bathroomsMin == this.property.bathroomsMax && this.property.bathroomsMin != null) {
      this.bathrooms.text = this.property.bathroomsMin.toString();
      this.bathrooms.speech = `Number of bathrooms: ${this.bathrooms.text} Bathrooms.`;
    }
    else {
      this.bathrooms.text = '?';
      this.bathrooms.speech = 'Unknown number of Bathrooms.';
    }
  }
}
