import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AngularMaterialModule } from '_components/angular-material.module';
import { RouterModule } from '@angular/router';

import { AdminPropertiesPage } from './admin-properties.page';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AngularMaterialModule,
    RouterModule.forChild([
      {
        path: 'admin/properties',
        component: AdminPropertiesPage
      }
    ])
  ],
  declarations: [AdminPropertiesPage]
})
export class AdminPropertiesPageModule {}
