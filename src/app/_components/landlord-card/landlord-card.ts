import { Input, Component } from '@angular/core';
import { Router } from '@angular/router'
import { ApiService, AuthenticationService } from '_services/index';

@Component({
  selector: 'landlord-card',
  templateUrl: './landlord-card.html',
  styleUrls: ['./landlord-card.scss'],
})
export class LandlordCardComponent {

  @Input('landlord')
  public landlord: any;
  
  @Input('context')
  public context: {
    search: {
      term: string,
    }
  };

  public aliases: string[];
  public aliasString: string;

  constructor(
    private router: Router,
    public authService: AuthenticationService,
    private apiService: ApiService,
  ) {
    this.aliases = [];
    this.aliasString = '';
  }
  ngOnInit(){
    if(this.landlord.hasOwnProperty('id')){
      this.getAliasesMatchingSearchTerm();
    }
  }

  getAliasesMatchingSearchTerm() {
    if(this.context){
      // Pull all aliases from landlord.Aliases and join them into a string for display if they match the search term
      for(let alias of this.landlord.Aliases){
  
        let searchTerm = this.context.search.term;
        
        if(alias.alias.toLowerCase().includes(searchTerm.toLowerCase())){
          this.aliases.push(alias.alias);
        }
      }
    
      this.aliasString = this.aliases.join(', ');
    }
  }
}
