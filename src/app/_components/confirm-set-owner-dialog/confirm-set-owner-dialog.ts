import { Component, Inject, ViewChild } from '@angular/core';
import { AlertService, ApiService, AuthenticationService } from '_services/index';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Property } from '_models/property.model';


@Component({
  selector: 'confirm-set-owner-dialog',
  templateUrl: './confirm-set-owner-dialog.html',
  styleUrls: ['./confirm-set-owner-dialog.scss'],
})
export class ConfirmSetOwnerDialog {
  @ViewChild('ngFormDirective') formDirective;
  form: FormGroup;
  ownable: any;
  ownableType: string;
  

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { ownable: any, ownableType: string },
    private apiService: ApiService,
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
  ) {

    this.ownable = data.ownable;
    this.ownableType = data.ownableType;
    this.form = this.formBuilder.group({
      AuthorId: [''],
    });
  }

  setOwner(){
    let ownableObject = {AuthorId: this.form.value.AuthorId};
    if(this.ownableType == 'property'){
      this.apiService.patchProperty(this.ownable.id, ownableObject).subscribe(res=>{
        this.alertService.success('Owner Changed Successfully');
      },
      err=> {
        
      });
    }
    else if(this.ownableType == 'landlord'){      
      this.apiService.patchLandlord(this.ownable.id, ownableObject).subscribe(res=>{
        this.alertService.success('Owner Changed Successfully');
      },
      err=> {      

      });
    }
  }

  removeOwner(){
    let ownableObject = {AuthorId: null}
    if(this.ownableType == 'property'){
      this.apiService.patchProperty(this.ownable.id, ownableObject).subscribe(res=>{
        this.alertService.success('Ownership Removed');
      },
      err=> {

      });
    }
    else if(this.ownableType == 'landlord'){
      this.apiService.patchLandlord(this.ownable.id, ownableObject).subscribe(res=>{
        this.alertService.success('Ownership Removed');
      },
      err=> {

      });
    }
  }
}
