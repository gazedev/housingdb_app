import { Component } from '@angular/core';
import { ApiService } from '_services/api.service';

@Component({
  selector: 'admin-dashboard-page',
  templateUrl: 'admin-dashboard.page.html',
  styleUrls: ['admin-dashboard.page.scss'],
})
export class AdminDashboardPage {

  public landlords: any;
  public properties: any;
  public locations: any;
  public landlordsMap: any;
  public reviews: any;
  public profiles: any;
  public breadcrumbs: any;

  constructor(
    private apiService: ApiService,
  ) {
    this.landlords = [];
    this.properties = [];
    this.locations = {};
    this.landlordsMap = {};
    this.reviews = [];
    this.profiles = [];
  }

  ngOnInit() {
    this.breadcrumbs = ['Admin'];
    this.loadLandlords();
    this.loadProperties()
    this.loadReviews();
    this.loadProfiles();
  }

  loadLandlords() {
    this.apiService.getLandlords().subscribe(res => {
      this.landlords = res.slice(0,5);
    },
    err => {
      console.log('error', err);
    });
  }

  loadProperties() {
    this.apiService.getProperties().subscribe(res => {
      this.properties = res.slice(0,5);
    },
    err => {
      console.log('error', err);
    });
  }

  loadReviews() {
    this.apiService.getAllReviews().subscribe(res => {
      this.reviews = res.slice(0,5);
    },
    err => {
      console.log('error', err);
    });
  }

  loadProfiles() {
    this.apiService.getAccounts().subscribe(res => {
      this.profiles = res.slice(0,5);
    },
    err => {
      console.log('error', err);
    });
  }
}
