import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService, HeadService } from '_services/index';
import { Landlord } from '_models/landlord.model';

@Component({
  selector: 'landlords-page',
  templateUrl: 'landlords.page.html',
  styleUrls: ['landlords.page.scss'],
})
export class LandlordsPage {

  loading: boolean = true;
  landlords: Landlord[];
  page: any;
  breadcrumbs: any;
  searchTerm: string = '';

  @ViewChild('ngFormDirective') formDirective;
  @ViewChild('searchInput') searchInput: ElementRef;
  @ViewChild('cards') cards: ElementRef;
  form: FormGroup;
  submitAttempt: boolean;
  currentlySubmitting: boolean;

  filtersOpen: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private headService: HeadService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.landlords = [];

    this.page = {
      offset: 0,
      size: 25,
    };

    this.submitAttempt = false;
    this.currentlySubmitting = false;
    this.form = this.formBuilder.group({
      search: [''],
      name: [''],
      phone: [''],
      email: [''],
    });
  }

  ngOnInit() {
    this.headService.setPageTitle('Landlords');
    this.getQueryParamsAndFilter();
    this.getLandlords(this.form.value);
    this.breadcrumbs = ['Landlords'];
  }

  ngOnDestroy() {
    this.headService.setPageTitle('');
  }

  pageUpdated($event) {
    this.page.offset = $event.pageIndex;
    this.page.size = $event.pageSize;
  }

  getLandlords(options = {}) {
    this.loading = true;
    this.apiService.getLandlords(options).subscribe(res => {
      this.landlords = res;
      this.loading = false;
    },
    err => {
      console.log('error');
      console.log(err);
      this.loading = false;
    });
  }

  toggleFilters() {
    if (this.filtersOpen === true) {
      this.filtersOpen = false;
    } else {
      this.filtersOpen = true;
      // shift focus to #searchInput
      setTimeout(()=>{ // this will make the execution after the above boolean has changed
        this.searchInput.nativeElement.focus();
      },0);
    }
  }

  resetForm() {
    this.form.reset();
    this.formDirective.resetForm();
    // if we're clearing the form, we're clearing the filters, so we re-submit
    this.submit();
  }

  resetPaginator(){
    this.page.offset = 0;
  }

  submit() {
    this.loading = true;
    this.resetPaginator();
    this.addParamsToUrl()
    this.getSearchTerm();
    this.getLandlords(this.form.value);

    // This only works if you click submit button... 
    // pressing 'Enter' to submit doesn't change the focus.
    setTimeout(()=>{ 
      this.cards.nativeElement.focus();
    },0);
  }

  getSearchTerm(){
    if(this.form.value.search !== ''){
      this.searchTerm = this.form.value.search;
    }
    else if(this.form.value.name !== ''){
      this.searchTerm = this.form.value.name;
    }
  }

  getQueryParamsAndFilter(){
    // If there are any query params, open the filters and pass the values into the form
    if(Object.keys(this.route.snapshot.queryParams).length){
      this.filtersOpen = true;

      this.route.queryParamMap.subscribe(params => {
        this.form.patchValue({
          search: params.get('search'),
          name: params.get('name'),
          phone: params.get('phone'),
          email: params.get('email'),
        });
      });
    }
  }

  addParamsToUrl(){
    let searchTerms: any = {};
    if(this.form.value.search){
      searchTerms.search = this.form.value.search;
    }

      if(this.form.value.name){
        searchTerms.name = this.form.value.name;
      }  
      if(this.form.value.phone){
        searchTerms.phone = this.form.value.phone;
      }
      if(this.form.value.email){
        searchTerms.email = this.form.value.email;
      }
    
    
    this.router.navigate([], {
      queryParams: searchTerms,
    });
  }
}
