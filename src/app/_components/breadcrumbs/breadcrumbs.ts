import { Input, Component } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'breadcrumbs',
  templateUrl: './breadcrumbs.html',
  styleUrls: ['./breadcrumbs.scss'],
})
export class BreadcrumbsComponent {
  @Input('path')
  public path: any;
  public current: string;

  constructor(
    private router: Router,
  ) {

  }

  ngOnChanges() {
    this.current = this.path.pop();
  }
}
