import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AngularMaterialModule } from '_components/angular-material.module';
import { RouterModule } from '@angular/router';

import { AdminDashboardPage } from './admin-dashboard.page';

@NgModule({
  imports: [
    CommonModule,
    AngularMaterialModule,
    RouterModule.forChild([
      {
        path: 'admin/dashboard',
        component: AdminDashboardPage,
      }
    ])
  ],
  declarations: [AdminDashboardPage],
})
export class AdminDashboardPageModule {}
